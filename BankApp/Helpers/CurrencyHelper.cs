﻿namespace BankApp.Helpers
{
   class CurrencyHelper
   {
      public static bool TryParseCurrency(string amount, out decimal amountCurrency)
      {
         decimal amountDecimal;
         if (decimal.TryParse(amount, out amountDecimal))
         {
            if ((amountDecimal * 100) % 1 == 0)
            {
               amountCurrency = amountDecimal;
               return true;
            }
         }

         amountCurrency = 0;
         return false;
      }
   }
}
