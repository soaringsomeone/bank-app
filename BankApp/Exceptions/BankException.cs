﻿using System;
using System.Runtime.Serialization;

namespace BankApp
{
   [Serializable]
   internal class BankException : Exception
   {
      public BankException()
      {
      }

      public BankException(string message) : base(message)
      {
      }

      public BankException(string message, Exception innerException) : base(message, innerException)
      {
      }

      protected BankException(SerializationInfo info, StreamingContext context) : base(info, context)
      {
      }
   }
}