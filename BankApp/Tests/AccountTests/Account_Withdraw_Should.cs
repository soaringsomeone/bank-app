﻿using System.Collections.Generic;
using Xunit;

namespace BankApp.Tests.AccountTests
{
   public class Account_Withdraw_Should
   {
      private Account _account;
      private string _username = "username";
      private decimal _defaultBalance = 100;
      private List<Transaction> _transactions = new List<Transaction>();

      public Account_Withdraw_Should()
      {
         _account = new Account(_username, _defaultBalance, _transactions);
      }

      [Fact]
      public void CorrectlySubtractAmountToBalance()
      {
         var amount = 50;
         var expectedBalance = _defaultBalance - amount;
         _account.Withdraw(amount);

         Assert.Equal(expectedBalance, _account.CheckBalance());
      }

      [Fact]
      public void GiveNegativeBalanceIfMoreThanCurrentBalance()
      {
         var amount = _defaultBalance + 50;
         _account.Withdraw(amount);

         Assert.True(_account.CheckBalance() < 0);
      }
   }
}
