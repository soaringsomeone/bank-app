﻿using System.Collections.Generic;
using Xunit;

namespace BankApp.Tests.AccountTests
{
   public class Account_Deposit_Should
   {
      private Account _account;
      private string _username = "username";
      private decimal _defaultBalance = 0;
      private List<Transaction> _transactions = new List<Transaction>();

      public Account_Deposit_Should()
      {
         _account = new Account(_username, _defaultBalance, _transactions);
      }

      [Fact]
      public void CorrectlyAddAmountToBalance()
      {
         var amount = 50;
         var expectedBalance = _defaultBalance + amount;
         _account.Deposit(amount);

         Assert.Equal(expectedBalance, _account.CheckBalance());
      }
   }
}
