﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace BankApp.Tests.AccountTests
{
   public class Account_PrintTransactionHistory_Should
   {
      private Account _account;
      private string _username = "username";
      private decimal _defaultBalance = 100;
      private List<Transaction> _transactions = new List<Transaction>();

      public Account_PrintTransactionHistory_Should()
      {
         _account = new Account(_username, _defaultBalance, _transactions);
      }

      [Fact]
      public void ReturnNoHistoryFoundIfNoTransactionsDone()
      {
         Assert.Equal(Account.NoTransactionHistory, _account.PrintTransactionHistory());
      }

      [Theory]
      [InlineData(1)]
      [InlineData(5)]
      public void ReturnLinesEqualToNumberOfTransactionsIfGreaterThan0(int numberTransactions)
      {
         var rand = new Random();

         for (int i = 0; i < numberTransactions; i++)
         {
            if (rand.Next(0, 2) == 0)
            {
               _account.Deposit(rand.Next(1, 50));
            }
            else
            {
               _account.Withdraw(rand.Next(1, 50));
            }
         }

         Assert.NotEqual(Account.NoTransactionHistory, _account.PrintTransactionHistory());

         var numberOfLines = _account.PrintTransactionHistory().Split("\n").Where(line => !line.Equals("")).Count();

         Assert.True(numberOfLines == numberTransactions);
      }
   }
}
