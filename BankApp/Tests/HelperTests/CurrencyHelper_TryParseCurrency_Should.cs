﻿using BankApp.Helpers;
using Xunit;

namespace BankApp.Tests.HelperTests
{
   public class CurrencyHelper_TryParseCurrency_Should
   {
      public CurrencyHelper_TryParseCurrency_Should()
      {
      }

      [Theory]
      [InlineData("test")]
      [InlineData("1a")]
      [InlineData("1.1.1")]
      public void ReturnFalseIfNotNumber(string testString)
      {
         decimal result;
         Assert.False(CurrencyHelper.TryParseCurrency(testString, out result));
      }

      [Fact]
      public void ReturnFalseIfNumberButNotValidCurrency()
      {
         var testString = "1.111";
         decimal result;
         Assert.False(CurrencyHelper.TryParseCurrency(testString, out result));
      }

      [Theory]
      [InlineData("0", 0)]
      [InlineData("1", 1)]
      [InlineData("-1", -1)]
      [InlineData("1.11", 1.11)]
      public void ReturnTrueAndReturnEquivalentDecimalAmountIfValidCurrencyNumber(string testString, decimal expectedResult)
      {
         decimal result;
         Assert.True(CurrencyHelper.TryParseCurrency(testString, out result));
         Assert.Equal(expectedResult, result);
      }
   }
}