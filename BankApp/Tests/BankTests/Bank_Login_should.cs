﻿using Xunit;

namespace BankApp.Tests.BankTests
{
   public class Bank_Login_Should
   {
      private Bank _bank;
      private string _username = "username";
      private string _password = "password";

      public Bank_Login_Should()
      {
         _bank = new Bank();
         _bank.AddAccount(_username, _password);
      }

      [Fact]
      public void ThrowBankExceptionIfWrongPassword()
      {
         var wrongPassword = _password + "wrong";
         Assert.Throws<BankException>(() => _bank.Login(_username, wrongPassword));
      }

      [Fact]
      public void ThrowBankExceptionIfUsernameDoesNotExist()
      {
         var wrongUsername = _username + "wrong";
         Assert.Throws<BankException>(() => _bank.Login(wrongUsername, _password));
      }

      [Fact]
      public void ReturnCorrectAccountIfUsernamePasswordCorrect()
      {
         Assert.Equal(typeof(Account), _bank.Login(_username, _password).GetType());
         Assert.Equal(_username, _bank.Login(_username, _password).username);
      }
   }
}
