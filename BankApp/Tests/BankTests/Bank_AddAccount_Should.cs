﻿using Xunit;

namespace BankApp.Tests.BankTests
{
   public class Bank_AddAccount_Should
   {
      private Bank _bank;
      private string _username = "username";
      private string _password = "password";

      public Bank_AddAccount_Should()
      {
         _bank = new Bank();
      }

      [Fact]
      public void AddAccountSuccessfullyIfFirstAccountAdded()
      {
         Assert.False(_bank.AccountExists(_username));
         _bank.AddAccount(_username, _password);
         Assert.True(_bank.AccountExists(_username));
      }

      [Fact]
      public void ThrowBankExceptionIfAccountUsernameAlreadyExists()
      {
         _bank.AddAccount(_username, _password);
         Assert.Throws<BankException>(() => _bank.AddAccount(_username, _password));
      }

      [Fact]
      public void AddAccountIfEmptyUsername()
      {
         var emptyUsername = "";
         _bank.AddAccount(emptyUsername, _password);
         Assert.True(_bank.AccountExists(emptyUsername));
      }

      [Fact]
      public void AddAccountIfEmptyPassword()
      {
         var emptyPassword = "";
         _bank.AddAccount(_username, emptyPassword);
         Assert.True(_bank.AccountExists(_username));
      }
   }
}
