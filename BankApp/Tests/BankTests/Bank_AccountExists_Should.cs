﻿using Xunit;

namespace BankApp.Tests.BankTests
{
   public class Bank_AccountExists_Should
   {
      private Bank _bank;
      private string _username = "username";
      private string _password = "password";

      public Bank_AccountExists_Should()
      {
         _bank = new Bank();
      }

      [Fact]
      public void ReturnFalseIfNoUsers()
      {
         Assert.False(_bank.AccountExists(_username));
      }

      [Fact]
      public void ReturnFalseIfUsernameNotAdded()
      {
         var nonenteredUsername = _username + "notEntered";
         _bank.AddAccount(_username, _password);
         Assert.False(_bank.AccountExists(nonenteredUsername));
      }

      [Fact]
      public void ReturnTrueIfUsernameExists()
      {
         _bank.AddAccount(_username, _password);
         Assert.True(_bank.AccountExists(_username));
      }
   }
}
