﻿using System;

namespace BankApp
{
   class Program
   {
      static void Main(string[] args)
      {
         Console.WriteLine("Enter only the number to execute corresponding command. Type 'quit' to exit program.");

         string command;
         bool quit = false;

         var bank = new Bank();
         var consoleSession = new ConsoleSession(bank);

         while (!quit)
         {
            if (consoleSession.LoggedIn())
            {
               ConsoleSession.PrintCommandsLoggedIn();
            }
            else
            {
               ConsoleSession.PrintCommandsLoggedOut();
            }

            command = Console.ReadLine();

            if (command.Equals("quit"))
            {
               quit = true;
               continue;
            }

            Console.WriteLine("");

            if (consoleSession.LoggedIn())
            {
               consoleSession.LoggedInCommands(command);
            }
            else
            {
               consoleSession.LoggedOutCommands(command);
            }

            Console.WriteLine("");
         }

         Console.WriteLine("Program has exited.");
      }
   }
}
