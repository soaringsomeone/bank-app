﻿namespace BankApp
{
   class Transaction
   {
      private TransactionTypeEnum transactionType;
      private decimal amount;

      public Transaction(TransactionTypeEnum transactionType, decimal amount)
      {
         this.transactionType = transactionType;
         this.amount = amount;
      }

      public string PrintHistory()
      {
         return string.Format("{0} for ${1}", transactionType.ToString(), amount);
      }
   }
}
