﻿using System.Collections.Generic;
using System.Linq;

namespace BankApp
{
   class Account
   {
      public string username;
      private decimal _balance;
      private List<Transaction> _transactions;
      public static string NoTransactionHistory = "No history found.\n";

      public Account(string username, decimal balance, List<Transaction> transactions)
      {
         this.username = username;
         this._balance = balance;
         this._transactions = transactions;
      }

      public void Deposit(decimal amount)
      {
         _balance += amount;

         var newTransaction = new Transaction(TransactionTypeEnum.Deposit, amount);

         AddTransaction(newTransaction);
      }

      public void Withdraw(decimal amount)
      {
         _balance -= amount;

         var newTransaction = new Transaction(TransactionTypeEnum.Withdraw, amount);

         AddTransaction(newTransaction);
      }

      public decimal CheckBalance()
      {
         return _balance;
      }

      public string PrintTransactionHistory()
      {
         var transactions = GetTransactions();

         if (!transactions.Any())
         {
            return NoTransactionHistory;
         }

         var history = "";

         foreach (var transaction in transactions)
         {
            history += transaction.PrintHistory() + "\n";
         }

         return history;
      }

      private void AddTransaction(Transaction transaction)
      {
         _transactions.Add(transaction);
      }

      private List<Transaction> GetTransactions()
      {
         return _transactions;
      }
   }
}
