﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace BankApp
{
   class Bank
   {
      private Dictionary<string, string> usernameSaltDictionary;
      private Dictionary<string, Account> loginAccountDictionary;

      public Bank()
      {
         usernameSaltDictionary = new Dictionary<string, string>();
         loginAccountDictionary = new Dictionary<string, Account>();
      }

      public void AddAccount(string username, string password)
      {
         if (AccountExists(username))
         {
            throw new BankException("Account username already exists.");
         }

         var salt = generateSalt();
         usernameSaltDictionary.Add(username, salt);

         var hashString = getHashedPhrase(username, password);
         var newAccount = new Account(username, 0, new List<Transaction>());

         loginAccountDictionary.Add(hashString, newAccount);

      }

      public bool AccountExists(string username)
      {
         return usernameSaltDictionary.ContainsKey(username);
      }

      public Account Login(string username, string password)
      {
         var hash = getHashedPhrase(username, password);
         Account account;

         if (!loginAccountDictionary.TryGetValue(hash, out account))
         {
            throw new BankException("Hashed phrase for " + username + " did not match.");
         }

         return account;
      }

      /// <summary>
      /// Generates a random string of 15 characters for salting the password. 
      /// </summary>
      /// <returns></returns>
      private string generateSalt()
      {
         var rand = new Random();
         var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
         var result = new char[15];

         for (int i = 0; i < result.Length; i++)
         {
            result[i] = chars[rand.Next(chars.Length)];
         }

         return new string(result);
      }

      private string getSaltedPhrase(string username, string password)
      {
         string salt;
         if (!usernameSaltDictionary.TryGetValue(username, out salt))
         {
            throw new BankException(username + " has no associated salt");
         }

         return username + password + salt;
      }

      /// <summary>
      /// https://stackoverflow.com/questions/3984138/hash-string-in-c-sharp
      /// </summary>
      /// <param name="username"></param>
      /// <param name="password"></param>
      /// <returns></returns>
      private string getHashedPhrase(string username, string password)
      {
         var saltedPhrase = getSaltedPhrase(username, password);
         HashAlgorithm algorithm = SHA256.Create();

         var hashBytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(saltedPhrase));

         var sb = new StringBuilder();

         foreach (var b in hashBytes)
         {
            // X2 formats it to uppercase hexadecimal
            sb.Append(b.ToString("X2"));
         }

         return sb.ToString();
      }
   }
}
