﻿using BankApp.Helpers;
using System;

namespace BankApp
{
   class ConsoleSession
   {
      private Bank bank;
      public Account activeAccount;

      public ConsoleSession(Bank bank)
      {
         this.bank = bank;
      }

      public static void PrintCommandsLoggedOut()
      {
         Console.WriteLine("(1) Add Account");
         Console.WriteLine("(2) Log in");
      }

      public static void PrintCommandsLoggedIn()
      {
         Console.WriteLine("(1) Deposit");
         Console.WriteLine("(2) Withdraw");
         Console.WriteLine("(3) Check Balance");
         Console.WriteLine("(4) See Transaction History");
         Console.WriteLine("(5) Log Out");
      }

      public void LoggedOutCommands(string command)
      {
         switch (command)
         {
            case "1":
               AddAccount();
               break;
            case "2":
               Login();
               break;
            default:
               Console.WriteLine("Cannot find command. Please enter valid command.");
               break;
         }
      }

      public void LoggedInCommands(string command)
      {
         switch (command)
         {
            case "1":
               Deposit();
               break;
            case "2":
               Withdraw();
               break;
            case "3":
               CheckBalance();
               break;
            case "4":
               SeeTransactionHistory();
               break;
            case "5":
               Logout();
               break;
            default:
               Console.WriteLine("Cannot find command. Please enter valid command.");
               break;
         }
      }

      public void AddAccount()
      {
         Console.WriteLine("Enter in new username");
         var username = Console.ReadLine();

         try
         {
            Console.WriteLine("Enter password");
            var password = Console.ReadLine();

            bank.AddAccount(username, password);
         }
         catch (Exception e)
         {
            Console.WriteLine(e.Message);
         }
      }

      public void Login()
      {
         Console.WriteLine("Enter username");
         var username = Console.ReadLine();
         Console.WriteLine("Enter Password");
         var password = Console.ReadLine();

         try
         {
            var account = bank.Login(username, password);

            activeAccount = account;
            Console.WriteLine("Logged in as " + username);
         }
         catch
         {
            Console.WriteLine("Could not log in as user " + username);
         }
      }

      public void Deposit()
      {
         Console.WriteLine("Enter amount to deposit:");
         var amount = Console.ReadLine();
         decimal amountCurrency;

         if (CurrencyHelper.TryParseCurrency(amount, out amountCurrency))
         {
            if (amountCurrency < 0)
            {
               Console.WriteLine("Cannot deposit a negative amount");
            }
            else
            {
               activeAccount.Deposit(amountCurrency);
               Console.WriteLine("Deposited $" + amount);
            }
         }
         else
         {
            Console.WriteLine(amount + " is not a valid monetary amount.");
         }
      }

      public void Withdraw()
      {
         Console.WriteLine("Enter amount to withdraw:");
         var amount = Console.ReadLine();
         decimal amountCurrency;

         if (CurrencyHelper.TryParseCurrency(amount, out amountCurrency))
         {
            if (amountCurrency < 0)
            {
               Console.WriteLine("Cannot withdraw a negative amount");
            }
            else
            {
               activeAccount.Withdraw(amountCurrency);
               Console.WriteLine("Withdrew $" + amount);
            }
         }
         else
         {
            Console.WriteLine(amount + " is not a valid monetary amount.");
         }
      }

      public void CheckBalance()
      {
         var balance = activeAccount.CheckBalance();
         var formattedBalance = balance < 0 ? "-$" + Math.Abs(balance) : "$" + balance;
         Console.WriteLine("Your current balance is " + formattedBalance);
      }

      public void SeeTransactionHistory()
      {
         Console.WriteLine(activeAccount.PrintTransactionHistory());
      }

      public void Logout()
      {
         activeAccount = null;
         Console.WriteLine("Successfully logged out. See you soon!");
      }

      public bool LoggedIn()
      {
         return activeAccount != null;
      }
   }
}
